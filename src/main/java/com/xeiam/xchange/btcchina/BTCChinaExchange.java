package com.xeiam.xchange.btcchina;

import com.xeiam.xchange.BaseExchange;
import com.xeiam.xchange.Exchange;
import com.xeiam.xchange.ExchangeSpecification;
import com.xeiam.xchange.btcchina.service.polling.BTCChinaAccountService;
import com.xeiam.xchange.btcchina.service.polling.BTCChinaMarketDataService;
import com.xeiam.xchange.btcchina.service.polling.BTCChinaTradeService;

public class BTCChinaExchange extends BaseExchange implements Exchange {
    public BTCChinaExchange() {

    }

    @Override
    public void applySpecification(ExchangeSpecification exchangeSpecification) {

        super.applySpecification(exchangeSpecification);
        this.pollingMarketDataService = new BTCChinaMarketDataService(exchangeSpecification);
        this.pollingTradeService = new BTCChinaTradeService(exchangeSpecification);
        this.pollingAccountService = new BTCChinaAccountService(exchangeSpecification);
    }

    @Override
    public ExchangeSpecification getDefaultExchangeSpecification() {
        ExchangeSpecification exchangeSpecification = new ExchangeSpecification(this.getClass().getCanonicalName());
        exchangeSpecification.setExchangeName("BTCChina");
        exchangeSpecification.setExchangeDescription("BTCChina is a Bitcoin exchange located in China.");
/*

        final Map<String, Object> exchangeSpecificParameters = new HashMap<String, Object>();
        exchangeSpecificParameters.put("dataSslUri", "https://data.btcchina.com");

        exchangeSpecification.setExchangeSpecificParameters(exchangeSpecificParameters);
*/

        return exchangeSpecification;
    }
}
