package com.xeiam.xchange.bitstamp;

import com.xeiam.xchange.BaseExchange;
import com.xeiam.xchange.Exchange;
import com.xeiam.xchange.ExchangeSpecification;
import com.xeiam.xchange.bitstamp.service.polling.BitstampAccountService;
import com.xeiam.xchange.bitstamp.service.polling.BitstampMarketDataService;
import com.xeiam.xchange.bitstamp.service.polling.BitstampTradeService;

/**
 * @author Matija Mazi
 */
public class BitstampExchange extends BaseExchange implements Exchange {

    @Override
    public ExchangeSpecification getDefaultExchangeSpecification() {
        ExchangeSpecification exchangeSpecification = new ExchangeSpecification(this.getClass().getCanonicalName());
        exchangeSpecification.setHost("www.bitstamp.net");
        exchangeSpecification.setExchangeName("Bitstamp");
        exchangeSpecification.setExchangeDescription("Bitstamp is a Bitcoin exchange registered in Slovenia.");

        return exchangeSpecification;
    }

    @Override
    public void applySpecification(ExchangeSpecification exchangeSpecification) {
        super.applySpecification(exchangeSpecification);
        this.pollingMarketDataService = new BitstampMarketDataService(exchangeSpecification);
        this.pollingTradeService = new BitstampTradeService(exchangeSpecification);
        this.pollingAccountService = new BitstampAccountService(exchangeSpecification);
    }
}