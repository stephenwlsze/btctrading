package com.tradingKing.lmax;

import com.lmax.disruptor.EventHandler;

/**
 * Created by stephensze on 13/4/14.
 */
public class TickDataHandler implements EventHandler<TickData> {
    public void onEvent(TickData event, long sequence, boolean endOfBatch) {
        System.out.println("Event: " + event.toString());
    }
}
