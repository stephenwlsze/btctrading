package com.tradingKing.lmax;

import com.lmax.disruptor.EventFactory;

/**
 * Created by stephensze on 13/4/14.
 */
public class TickDataFactory implements EventFactory<TickData> {

    public TickData newInstance() {
        return new TickData();
    }
}