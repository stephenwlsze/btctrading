package com.tradingKing.lmax;

import com.lmax.disruptor.EventTranslatorOneArg;
import com.lmax.disruptor.RingBuffer;
import com.xeiam.xchange.dto.marketdata.Ticker;

import java.math.BigDecimal;
import java.nio.ByteBuffer;

/**
 * Created by stephensze on 13/4/14.
 */
public class TickDataProducer {
    private final RingBuffer<TickData> ringBuffer;

    public TickDataProducer(RingBuffer<TickData> ringBuffer) {
        this.ringBuffer = ringBuffer;
    }

    public static final EventTranslatorOneArg<TickData, Ticker> TRANSLATOR =
            new EventTranslatorOneArg<TickData, Ticker>() {
                public void translateTo(TickData event, long sequence, Ticker ticker) {
                    event.set(ticker.getCurrencyPair(), ticker.getLast(), ticker.getBid(), ticker.getAsk(), ticker.getHigh(), ticker.getLow(), ticker.getVolume(), ticker.getTimestamp());
                }
            };

    public void onData(Ticker ticker) {
        ringBuffer.publishEvent(TRANSLATOR, ticker);
    }
}