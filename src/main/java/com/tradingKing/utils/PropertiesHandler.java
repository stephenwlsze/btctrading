package com.tradingKing.utils;

import java.io.IOException;
import java.util.Properties;

/**
 * Created by stephensze on 13/4/14.
 */
public class PropertiesHandler {
    private static volatile PropertiesHandler propertiesHandler;
    private static volatile Properties props;

    private PropertiesHandler() {
        props = new Properties();
        try {
            props.load(PropertiesHandler.class.getResourceAsStream("/system.properties"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //double locking singleton
    private static PropertiesHandler getInstance() {
        if (propertiesHandler == null) {
            synchronized (PropertiesHandler.class) {
                if (propertiesHandler == null) {
                    propertiesHandler = new PropertiesHandler();
                }
            }
        }
        return propertiesHandler;
    }

    public static String getProperty(String key) {
        return getInstance().props.getProperty(key);
    }
}

