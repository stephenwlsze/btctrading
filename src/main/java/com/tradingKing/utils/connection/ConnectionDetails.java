package com.tradingKing.utils.connection;

import com.xeiam.xchange.Exchange;

public interface ConnectionDetails {
    public Exchange createExchange();
}
