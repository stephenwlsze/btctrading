package com.tradingKing.utils.connection;

import com.tradingKing.utils.Account;
import com.tradingKing.utils.PropertiesHandler;
import com.xeiam.xchange.Exchange;
import com.xeiam.xchange.ExchangeFactory;
import com.xeiam.xchange.ExchangeSpecification;
import com.xeiam.xchange.anx.v2.ANXExchange;

import java.io.IOException;

public class ANXConnectionDetails implements ConnectionDetails{
    public Exchange createExchange(){
        ExchangeSpecification exSpec = new ExchangeSpecification(ANXExchange.class);
        try{
            Account account = new Account(PropertiesHandler.getProperty("anx.account.details.file.name"));
            exSpec.setApiKey(account.getApiKey());
            exSpec.setSecretKey(account.getSecret());
            exSpec.setSslUri(account.getUrl());
            exSpec.setPort(Integer.parseInt(account.getPort()));

            return ExchangeFactory.INSTANCE.createExchange(exSpec);
        } catch (IOException e){
            System.out.println("ANX account details cannot be found. Disabling the ANX function.");
            return null;
        }
    }
}
