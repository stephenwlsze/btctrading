package com.tradingKing.utils.connection;

import com.tradingKing.utils.Account;
import com.tradingKing.utils.PropertiesHandler;
import com.xeiam.xchange.Exchange;
import com.xeiam.xchange.ExchangeFactory;
import com.xeiam.xchange.ExchangeSpecification;
import com.xeiam.xchange.btcchina.BTCChinaExchange;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class BTCChinaConnectionDetails implements ConnectionDetails{
    public Exchange createExchange(){
        ExchangeSpecification exSpec = new ExchangeSpecification(BTCChinaExchange.class);
        try{
            Account account = new Account(PropertiesHandler.getProperty("btcchina.account.details.file.name"));
            exSpec.setApiKey(account.getApiKey());
            exSpec.setSecretKey(account.getSecret());
            exSpec.setSslUri(account.getUrl());
            exSpec.setPort(Integer.parseInt(account.getPort()));

            final Map<String, Object> exchangeSpecificParameters = new HashMap<String, Object>();
            exchangeSpecificParameters.put("dataSslUri", "https://data.btcchina.com");

            exSpec.setExchangeSpecificParameters(exchangeSpecificParameters);

            return ExchangeFactory.INSTANCE.createExchange(exSpec);
        } catch (IOException e){
            System.out.println("BTC China account details cannot be found. Disabling the BTC China function.");
            return null;
        }
    }
}
