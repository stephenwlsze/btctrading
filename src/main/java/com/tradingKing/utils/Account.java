package com.tradingKing.utils;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class Account {
    private String url;
    private String port;
    private String apiKey;
    private String secret;

    public Account(String accountFileName) throws IOException{
        readAccount(accountFileName);
    }
    public Account(String url, String port, String apiKey, String secret){
        this.url = url;
        this.port = port;
        this.apiKey = apiKey;
        this.secret = secret;
    }

    public String getUrl(){
        return url;
    }

    public String getApiKey(){
        return apiKey;
    }

    public String getSecret(){
        return secret;
    }

    public String getPort(){
        return port;
    }

    private void readAccount(String accountFileName) throws IOException{
        FileReader accountFile = new FileReader(Constants.ACCOUNT_DETAILS_LOCATION + "/" + accountFileName);

        try(BufferedReader reader = new BufferedReader(accountFile)){
            this.url = reader.readLine();
            this.port = reader.readLine();
            this.apiKey = reader.readLine();
            this.secret = reader.readLine();
        }
    }
}
