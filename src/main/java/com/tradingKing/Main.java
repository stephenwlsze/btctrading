package com.tradingKing;

import com.lmax.disruptor.BlockingWaitStrategy;
import com.lmax.disruptor.RingBuffer;
import com.lmax.disruptor.dsl.Disruptor;
import com.lmax.disruptor.dsl.ProducerType;
import com.tradingKing.lmax.TickData;
import com.tradingKing.lmax.TickDataFactory;
import com.tradingKing.lmax.TickDataHandler;
import com.tradingKing.lmax.TickDataProducer;
import com.tradingKing.utils.connection.ANXConnectionDetails;
import com.tradingKing.utils.connection.BitstampConnectionDetails;
import com.xeiam.xchange.Exchange;
import com.xeiam.xchange.currency.CurrencyPair;
import com.xeiam.xchange.dto.marketdata.Ticker;
import com.xeiam.xchange.service.polling.PollingMarketDataService;

import java.nio.ByteBuffer;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

/**
 * Created by stephensze on 7/4/14.
 */
public class Main {

    public static void main(String[] args) throws Exception {
        // Executor that will be used to construct new threads for consumers
        Executor executor = Executors.newCachedThreadPool();

        // The factory for the event
        TickDataFactory factory = new TickDataFactory();

        // Specify the size of the ring buffer, must be power of 2.
        int bufferSize = 2048;

        // Construct the Disruptor
        Disruptor<TickData> disruptor = new Disruptor( factory,
                bufferSize,
                executor,
                ProducerType.SINGLE,
                new BlockingWaitStrategy()
        );

        // Connect the handler
        disruptor.handleEventsWith(new TickDataHandler());

        // Start the Disruptor, starts all threads running
        disruptor.start();

        // Get the ring buffer from the Disruptor to be used for publishing.
        RingBuffer<TickData> ringBuffer = disruptor.getRingBuffer();

        TickDataProducer tickDataProducer = new TickDataProducer(ringBuffer);

        Exchange exchange = (new ANXConnectionDetails()).createExchange();
        PollingMarketDataService marketDataService = exchange.getPollingMarketDataService();
        while(true){
            long start = System.currentTimeMillis();
            System.out.println("System time at start: " + start);
            Ticker ticker = marketDataService.getTicker(new CurrencyPair("BTC","HKD"));
            long end = System.currentTimeMillis();
            System.out.println("Time for getting data= " + (end-start));
            tickDataProducer.onData(ticker);
            System.out.println("whole process = " + (System.currentTimeMillis() - start));
            Thread.sleep(1000);
        }
    }
}
