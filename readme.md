## Introduction

This project is a skeleton for using LMAX (https://github.com/LMAX-Exchange/disruptor) in a Bitcoin trading system.

It also provide some basic connection to exchanges using the XChange library (https://github.com/timmolter/XChange).